package com.cosi.javatest.service;

import com.cosi.javatest.dto.UserVariables;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Service class for simple arithmetic calculations
 *
 * @author Shevchenko Igor
 * @since 2016-08-07
 */
@Service
public class CalculatorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CalculatorService.class);

    @Value("${calculator.formula}")
    private String formula;

    /**
     * Method which performs simple arithmetic calculations
     * @param userVariables List of variables
     * @return double Result of calculation
     */
    @Async
    public Future<Double> calculate(UserVariables userVariables){
        Map<String, Double> variables = userVariables.getVariables();
        LOGGER.info("Task Calculation of variables={} with formula={}", variables, formula);

        Expression e = new ExpressionBuilder(formula)
                .variables(variables.keySet())
                .build();

        for (Map.Entry<String, Double> entry : variables.entrySet()) {
            e.setVariable( entry.getKey(), entry.getValue());
        }

        double result = e.evaluate();

        LOGGER.info("Task Calculation with formula={}" + formula + " completed");
        return new AsyncResult<>(result);
    }
}
