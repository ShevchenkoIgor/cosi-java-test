package com.cosi.javatest.dto;

import java.util.Map;

/**
 * DTO class for request
 *
 * @author Shevchenko Igor
 * @since 2016-08-07
 */
public class UserVariables {

    private Map<String, Double> variables;

    public Map<String, Double> getVariables() {
        return variables;
    }

    public void setVariables(Map<String, Double> variables) {
        this.variables = variables;
    }

    @Override
    public String toString() {
        return "UserVariables{" +
                "variables=" + variables +
                '}';
    }
}
