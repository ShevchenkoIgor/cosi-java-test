package com.cosi.javatest.dto;

import java.util.List;

/**
 * DTO class for request
 *
 * @author Shevchenko Igor
 * @since 2016-08-11
 */
public class Variables {

    private List<UserVariables> userVariables;

    public List<UserVariables> getUserVariables() {
        return userVariables;
    }

    public void setUserVariables(List<UserVariables> userVariables) {
        this.userVariables = userVariables;
    }

    @Override
    public String toString() {
        return "Variables{" +
                "userVariables=" + userVariables +
                '}';
    }
}
