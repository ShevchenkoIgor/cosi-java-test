package com.cosi.javatest.controller;

import com.cosi.javatest.dto.UserVariables;
import com.cosi.javatest.dto.Variables;
import com.cosi.javatest.service.CalculatorService;
import com.cosi.javatest.validator.VariableValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Controller class with Rest endpoint
 *
 * @author Shevchenko Igor
 * @since 2016-08-07
 */
@RequestMapping("/api/calc")
@RestController
public class CalculatorController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CalculatorController.class);

    private final CalculatorService calculatorService;
    private final VariableValidator variableValidator;

    @Autowired
    public CalculatorController(CalculatorService calculatorService, VariableValidator variableValidator) {
        this.calculatorService = calculatorService;
        this.variableValidator = variableValidator;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(variableValidator);
    }

    /**
     * Method allows to calculate different set of data in parallel
     * @param variables List of variables which needs to be calculated
     * @param bindingResult
     * @return List with calculation results
     * @throws Exception
     */
    @RequestMapping(method = POST)
    public ResponseEntity getUser(@Valid @RequestBody final Variables variables, BindingResult bindingResult) throws Exception {
        LOGGER.info("Calculation was started with variables={}, bindingResult={}", variables, bindingResult);
        if (bindingResult.hasErrors()) {
            return ResponseEntity.badRequest().body(bindingResult.getAllErrors());
        }

        List<Double> results = new ArrayList<>();

        for(UserVariables userVariables: variables.getUserVariables()) {
            results.add(calculatorService.calculate(userVariables).get());
        }

        LOGGER.info("Calculation was finished with result={}", results);
        return new ResponseEntity<>(results, HttpStatus.OK);
    }
}
