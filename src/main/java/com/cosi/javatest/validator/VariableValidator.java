package com.cosi.javatest.validator;

import com.cosi.javatest.dto.UserVariables;
import com.cosi.javatest.dto.Variables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.List;

/**
 * Validator for request payload
 *
 * @author Shevchenko Igor
 * @since 2016-08-07
 */
@Component
public class VariableValidator implements Validator{
    private static final Logger LOGGER = LoggerFactory.getLogger(VariableValidator.class);

    @Value("${calculator.formula}")
    private String formula;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(Variables.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        LOGGER.info("Validating {}", target);
        Variables variables = (Variables) target;

        if (variables.getUserVariables() == null) {
            errors.reject("userVariables", "Field can't be null");
            return;
        }

        if (variables.getUserVariables().isEmpty()) {
            errors.reject("userVariables", "Field can't be empty");
            return;
        }

        for(UserVariables variable : variables.getUserVariables()) {
            if (variable.getVariables() == null) {
                errors.reject("variables", "Field can't be null");
                return;
            }

            if (variable.getVariables().isEmpty()) {
                errors.reject("variables", "Field can't be empty");
                return;
            }

            for (String key : variable.getVariables().keySet()) {
                LOGGER.info("Validating key = {}", key);
                if (!formula.contains(key)) {
                    errors.reject("variables", key + " variable doesn't match with variables from formula " + formula);
                }
            }
        }
    }
}
