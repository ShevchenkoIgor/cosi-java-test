package com.cosi.javatest.exception;

import com.cosi.javatest.CalculatorApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;

import java.lang.reflect.Method;

/**
 * Exception Handler for async execution
 *
 * @author  Shevchenko Igor
 * @since   2016-08-11
 */
public class CalculatorAsyncExceptionHandler implements AsyncUncaughtExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(CalculatorAsyncExceptionHandler.class);

    @Override
    public void handleUncaughtException(Throwable throwable, Method method, Object... obj) {
        StringBuilder error = new StringBuilder();

        error.append("Exception message - " + throwable.getMessage());
        error.append("Method name - " + method.getName());
        for (Object param : obj) {
            error.append("Parameter value - " + param);
        }

        LOGGER.error(error.toString());
    }
}
