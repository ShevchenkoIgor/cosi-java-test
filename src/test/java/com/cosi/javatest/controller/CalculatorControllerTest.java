package com.cosi.javatest.controller;


import com.cosi.javatest.dto.UserVariables;
import com.cosi.javatest.service.CalculatorService;
import com.cosi.javatest.validator.VariableValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(CalculatorController.class)
public class CalculatorControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CalculatorService calculatorServiceMock;

    @SpyBean
    private VariableValidator variableValidator;

    @Test
    public void calculate_NullValidation() throws Exception {
        String userVariablesJson = "{\n" +
                "  \"userVariables\":\n" +
                "  [{\n" +
                "  \"variables\":null" +
                "  }]\n" +
                "}";

        MvcResult result = this.mockMvc.perform(post("/api/calc")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userVariablesJson)
        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$[0].defaultMessage", is("Field can't be null")))
                .andReturn();

        String content = result.getResponse().getContentAsString();
        assertNotNull(content);
    }

    @Test
    public void calculate_EmptyValidation() throws Exception {
        String userVariablesJson = "{\n" +
                "  \"userVariables\":\n" +
                "  [{\n" +
                "  \"variables\":\n" +
                "  { \n" +
                "   }\n" +
                "  }]\n" +
                "}";


        MvcResult result = mockMvc.perform(post("/api/calc")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userVariablesJson)
        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$[0].defaultMessage", is("Field can't be empty")))
                .andReturn();

        String content = result.getResponse().getContentAsString();
        assertNotNull(content);
    }

    @Test
    public void calculate_WrongContentValidation() throws Exception {
        String userVariablesJson = "{\n" +
                "  \"userVariables\":\n" +
                "  [{\n" +
                "  \"variables\":\n" +
                "  { \n" +
                "        \"x1\" : 10.0, \n" +
                "        \"x3\" : 20.0\n" +
                "   }\n" +
                "  }]\n" +
                "}";

        MvcResult result = mockMvc.perform(post("/api/calc")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userVariablesJson)
        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$[0].defaultMessage", is("x3 variable doesn't match with variables from formula (x1 + 5) * x2 / x1")))
                .andReturn();

        String content = result.getResponse().getContentAsString();
        assertNotNull(content);
    }
}
