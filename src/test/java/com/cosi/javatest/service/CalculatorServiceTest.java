package com.cosi.javatest.service;

import com.cosi.javatest.dto.UserVariables;
import net.objecthunter.exp4j.tokenizer.UnknownFunctionOrVariableException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static junit.framework.TestCase.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CalculatorServiceTest {

    @Autowired
    private CalculatorService calculatorService;

    private UserVariables userVariables;

    @Before
    public void setup() {
        userVariables = new UserVariables();
    }

    @Test
    public void calculate() throws ExecutionException, InterruptedException {
        Map<String, Double> map = new HashMap<>();
        map.put("x1", 10d);
        map.put("x2", 20d);
        userVariables.setVariables(map);
        Future<Double> result = calculatorService.calculate(userVariables);
        assertEquals(30.0, result.get());
    }
}
