**Checkout:**

Attached jar file target\cosi-java-test.jar to run application.

Copy jar to any place.

**Build application:**

cd to project directory.

run **_mvn clean install_** command to build jar file.

**Run Spring boot application**

run java -jar {jar_path}/cosi-java-test.jar

Aplication is available by http://localhost:8081/api/calc . 

Using Advanced Rest Client tool test endpoint _POST /api/calc_ with payload:

`{
  "variables":
  { 
        "x1" : 10.0, 
        "x2" : 20.0
   }
}`

**Test:** 

Application can be tested with tests: CalculatorControllerTest and CalculatorServiceTest

run **_mvn test _** to start tests

